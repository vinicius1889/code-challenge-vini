import { Message } from "./Message";

export interface IMessageRepo {
  findLastMessages(last: number): Message[];
}
