import { MessageRepo } from "../../../src/infrastructure/repo/MessageRepo";

describe("MessageRepo tests", () => {
  test("should create a new MessageRepo Instance from genesis message", () => {
    const messageRepo = MessageRepo.buildInstanceFromGenesis();

    expect(messageRepo).toBeDefined();
  });

  test("should create a new MessageRepo Instance from random message", () => {
    const messageRepo = MessageRepo.buildInstanceFromRandomMessage();

    expect(messageRepo).toBeDefined();
  });

  test("should return the first block (genesis)", () => {
    const messageRepo = MessageRepo.buildInstanceFromRandomMessage();
    const genesisMessage = messageRepo.findFirstsMessages(1);

    expect(genesisMessage[0].isGenesis()).toBeTruthy();
  });

  test("should take the last 5 items", () => {
    const messageRepo = MessageRepo.buildInstanceFromRandomMessage();
    const last5Messages = messageRepo.findLastMessages(5);

    expect(last5Messages.length).toBe(5);
    expect(last5Messages[0].isGenesis()).toBeFalsy();
    expect(last5Messages[0].checkChildMessage(last5Messages[1])).toBeTruthy();
    expect(
      last5Messages[4].checkParentMessageID(last5Messages[3].id)
    ).toBeTruthy();
  });
});
