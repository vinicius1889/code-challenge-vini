# Code challenge

Hey guys, this is a simple solution for the code challenge.

I've solved the challenge using the DDD approach. You can check the layers of infra, domain, and application. The API layer is not done because I believe this part of the challenge was not so meaningful to show. All the other layers were done including query and handlers.
Even so, my suggestion for a simple Rest API should be a serverless solution (serverless framework) using IoC (Inversify). This is the normal approach for a simple and not coupled Rest API.

The scope of the tests was reduced in order to create a simple solution. Day-by-day, much more tests are needed, but for non-production code, I think it's good enough.

Thank you!!
