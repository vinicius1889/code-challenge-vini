import { Body, ID } from "../common";
import { hashString } from "../infrastructure/hash";

export class Message {
  private constructor(private _body: Body, private _id: ID) {}

  get body(): Body {
    return this._body;
  }

  get id(): ID {
    return this._id;
  }

  /**
   * Create new message regarding the hash contraint
   * The body of the new message will be informed and the ID of the previous message
   * should be passed in order to create the proper chain
   * @param body
   * @param previousId
   * @returns
   */
  static createNewMessage(body: Body, previousId: ID): Message {
    return new Message(body, previousId);
  }

  /**
   * Create a new instance
   * @param body
   * @param previousId
   * @returns
   */
  static getInstance(body: Body, previousId: ID): Message {
    return new Message(body, previousId);
  }

  /**
   * This method will validate if the parentID is correct
   * @param parentId
   * @returns
   */
  public checkParentMessageID(parentId: ID): boolean {
    return (
      hashString(parentId.content + hashString(this._body.content)) ===
      this._id.content
    );
  }

  /**
   * This method will validate if the param message is a child
   * it means, if the message derive from the current ID
   * @param message
   * @returns
   */
  public checkChildMessage(message: Message): boolean {
    return (
      hashString(this._id.content + hashString(message._body.content)) ===
      message._id.content
    );
  }

  /**
   * Check if the message is the first one
   */
  public isGenesis(): boolean {
    return hashString(hashString(this._body.content)) === this._id.content;
  }
}
