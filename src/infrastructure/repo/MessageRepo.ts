import { Body, ID } from "../../common";
import { IMessageRepo } from "../../domain/IMessageRepo";
import { Message } from "../../domain/Message";
import MESSAGES_JSON from "./messages.json";

export class MessageRepo implements IMessageRepo {
  private sortedMessages: Message[];

  private constructor(sortedMessages: Message[]) {
    this.sortedMessages = sortedMessages;
  }

  /**
   * Build a new instance with all the mesages inside the messages.json
   */
  public static buildInstanceFromGenesis(): MessageRepo {
    const allMessages = MessageRepo.createMessagesFromJSON();
    let genesis = undefined;
    const chain: Message[] = [];

    for (let i = 0; i < allMessages.length - 1; i++) {
      if (allMessages[i].isGenesis()) {
        genesis = allMessages[i];
        allMessages.splice(i, 1);
        break;
      }
    }

    if (!genesis) {
      throw new Error("genesis not found");
    }

    chain.push(genesis);

    do {
      const current = allMessages.shift();

      if (!current) break;

      if (chain[chain.length - 1].checkChildMessage(current)) {
        chain.push(current);

        continue;
      }

      allMessages.push(current);
    } while (allMessages.length > 0);

    if (chain.length == 0) {
      throw new Error("Chain is empty");
    }

    return new MessageRepo(chain);
  }

  public static buildInstanceFromRandomMessage(): MessageRepo {
    const allMessages = MessageRepo.createMessagesFromJSON();
    const chain: Message[] = [];

    if (!allMessages[0].isGenesis()) {
      chain.push(allMessages.shift()!);
    } else {
      chain.push(allMessages.pop()!);
    }

    do {
      const current = allMessages.shift();

      if (!current) break;

      if (chain[chain.length - 1].checkChildMessage(current)) {
        chain.push(current);

        continue;
      }
      if (chain[0].checkParentMessageID(current.id)) {
        chain.unshift(current);

        continue;
      }

      allMessages.push(current);
    } while (allMessages.length > 0);

    if (chain.length == 0) {
      throw new Error("Chain is empty");
    }

    return new MessageRepo(chain);
  }

  private static createMessagesFromJSON(): Message[] {
    return [...MESSAGES_JSON].map((message) => {
      const body: Body = { content: message.encrypted };
      const id: ID = { content: message.id };

      return Message.getInstance(body, id);
    });
  }

  /**
   * Find the n last messages. If no messages was found the empty array will be returned
   * @param last
   */
  public findLastMessages(last: number): Message[] {
    const messages = [...this.sortedMessages];
    if (last >= messages.length) {
      return messages;
    }

    return messages.slice(messages.length - last);
  }

  public findFirstsMessages(num: number): Message[] {
    const messages = [...this.sortedMessages];
    if (num >= messages.length) {
      return messages;
    }

    return messages.slice(0, num);
  }
}
