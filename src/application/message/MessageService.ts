import { Message } from "../../domain/Message";
import { MessageRepo } from "../../infrastructure/repo/MessageRepo";

export class MessageService {
  constructor(private repo: MessageRepo) {}

  public findLastMessages(last: number): Message[] {
    return this.repo.findLastMessages(last);
  }
}
