import { Message } from "../../../domain/Message";
import { MessageService } from "../MessageService";
import { QueryLastMessages } from "../query/QueryLastMessages";

export class QueryLastMessagesHandler {
  constructor(private service: MessageService) {}

  public handle(query: QueryLastMessages): Message[] {
    return this.service.findLastMessages(query.last);
  }
}
