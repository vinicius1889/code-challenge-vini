import { sha256 } from "js-sha256";

/**
 * Function hashes the string using sha256 and returning the hex code
 * @param str
 * @returns
 */
export const hashString = (str: string): string => {
  const hashInstance = sha256.create();

  return hashInstance.update(str).hex();
};
