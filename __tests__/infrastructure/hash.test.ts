import { Body, ID } from "../../src/common";
import { Message } from "../../src/domain/Message";
import { hashString } from "../../src/infrastructure/hash";
import MESSAGES_JSON from "../testData/messages-2.json";

describe("Hash Tests", () => {
  test("should display the correct value in order to validate if the SHA implementation is the correct one", () => {
    expect(hashString("abcd")).toEqual(
      "88d4266fd4e6338d13b845fcf289579d209c897823b9217da3e161936f031589"
    );
  });

  test("should hash the combination of strings correctly", () => {
    const genersisID: ID = {
      content:
        "2077329bbeb9ed492dafd91cc7b06432703fdcdd36ec2d2b5c989fca44d75d73",
    };
    const allMessages: Message[] = MESSAGES_JSON.map((message) => {
      const body: Body = { content: message.encrypted };
      const id: ID = { content: message.id };

      return Message.getInstance(body, id);
    });
    const genesis = allMessages.find((m) => m.isGenesis());

    expect(genesis!.id.content).toBe(genersisID.content);
  });
});
