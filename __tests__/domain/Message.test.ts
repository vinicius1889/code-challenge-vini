import { Body, ID } from "../../src/common";
import { Message } from "../../src/domain/Message";
import _MESSAGES_JSON from "../testData/messages-2.json";

describe("Message tests", () => {
  describe("Default Message Tests", () => {
    const body: Body = {
      content:
        "eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0..qE_9ZTY_QqkwKPiIU3RalA.rvJf2a1Icu43GfUTb-xy5Jf2vZ8NPOsdhpDwaKT0U7k.Q4BUQ9103ZPMHpp5Oc749A",
    };
    const id: ID = {
      content:
        "2077329bbeb9ed492dafd91cc7b06432703fdcdd36ec2d2b5c989fca44d75d73",
    };
    const message: Message = Message.getInstance(body, id);

    test("should create a new instance and validate the correct parent ID", () => {
      expect(message.isGenesis()).toBeTruthy();
    });
  });
});
